<?php
/**
 * Plugin Name: TTN Berrikuntzak Carousel Widget
 * Description: Diska berrien carousel widgeta
 * Version: 0.0.4
 * Author: Jimakker
 * Author URI: http://twitter.com/jimakker
 */


add_action( 'widgets_init', 'ttn_berrikuntzak_carousel' );


function ttn_berrikuntzak_carousel() {
	register_widget( 'TTN_Berrikuntzak_Carousel_Widget' );
}

class TTN_Berrikuntzak_Carousel_Widget extends WP_Widget {

	function __construct() {
		$widget_ops = array( 'classname' => 'ttn-berrikuntzak-carousel', 'description' => __('Berrikuntzen carousel widdgeta', 'TTN') );

		$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'ttn-berrikuntzak-carousel-widget' );

		parent::__construct( 'ttn-berrikuntzak-carousel-widget', __('TTN Berrikuntzak Carousel', 'TTN'), $widget_ops, $control_ops );
	}

	function widget( $args, $instance ) {
		extract( $args );


        $args = array( 'posts_per_page' => 10, 'post_type' => 'diskak'); // azken 10 berrikuntzak
        $berrikuntzak = get_posts( $args );
        $brkntz_num = 0;

        echo $before_widget;
		?>

<h3 style="background:#222;width:100%;color:#fff;text-align:center;margin:0;">BERRIKUNTZAK</h3>
<div id="berrikuntzak-carousel" style="overflow:hidden;" class="carousel slide" data-ride="carousel" data-interval="10000">

    <div class="carousel-inner" role="listbox">

        <?php

        foreach( $berrikuntzak as $berrikuntza ) :
            $brkntz_num++;
            $post_thumbnail_id = get_post_thumbnail_id($berrikuntza->ID);
            $featured_src = wp_get_attachment_image_src( $post_thumbnail_id, 'medium' );
        ?>

            <div class="<?php if($brkntz_num == 1){ echo 'active'; } ?> item">
                <a href="<?php echo get_the_permalink($berrikuntza->ID); ?>" style="display:block;background-image:url(<?php echo $featured_src[0]; ?>);background-position:center;background-size:cover;width:100%;height:200px;" rel="bookmark" title="<?php echo $berrikuntza->post_title; ?>"></a>

            </div>

        <?php endforeach; ?>

    </div>

    <a class="left carousel-control" href="#berrikuntzak-carousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Aurrekoa</span>
    </a>

    <a class="right carousel-control" href="#berrikuntzak-carousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" style="left:0;"></span>
        <span class="sr-only">Hurrengoa</span>
    </a>

</div>


























		<?php
		echo $after_widget;
	}

	//Update the widget

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;


		return $instance;
	}


	function form( $instance ) {

		//Set up some default widget settings.
		$defaults = array( 'title' => __('TTN Berrikuntzak Carousel', 'TTN'), 'limit' => __('10', 'TTN'), 'show_info' => true );
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>



	<?php
	}
}

?>
